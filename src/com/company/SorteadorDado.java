package com.company;

import java.util.Random;

public class SorteadorDado{

    int faceSorteada;
    int faceSorteada2;
    int faceSorteada3;
    int somaFaces;

    int quantidadeFaces;

      public void sortearFace(int repeticoes){
        InputOutput io = new InputOutput();
        quantidadeFaces =  io.questionarFacesDadoCustomizavel();
        Dado dado = new Dado(quantidadeFaces);

        Random random = new Random();

        for(int i=0; i < repeticoes; i++) {

            faceSorteada = (random.nextInt(dado.getFaces())) + 1;
            faceSorteada2 = (random.nextInt(dado.getFaces())) + 1;
            faceSorteada3 = (random.nextInt(dado.getFaces())) + 1;
            somaFaces = faceSorteada + faceSorteada2 + faceSorteada3;

            io.exibirMensagem("As faces sorteadas foram: " + faceSorteada + "," + faceSorteada2 + "," + faceSorteada3 + " e a soma delas é " + somaFaces);
        }
        }
}
