package com.company;

import java.util.Scanner;

public class InputOutput {

    public int questionarFacesDadoCustomizavel(){

        int quantidadeFaces;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o número de faces desejado:");
        quantidadeFaces = scanner.nextInt();

        return quantidadeFaces;
    }

    public void exibirMensagem(String mensagem){
        System.out.println(mensagem);
    }
}
