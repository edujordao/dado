package com.company;

public class Dado {

    int faces;

    public Dado(int faces) {
        this.faces = faces;
    }

    public int getFaces() {
        return faces;
    }

    public void setFaces(int faces) {
        this.faces = faces;
    }
}
